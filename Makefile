# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: juveron <juveron@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/03/18 13:29:16 by juveron           #+#    #+#              #
#    Updated: 2019/05/02 12:58:16 by juveron          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fractol

CC = clang

FLAGS = -Wall -Werror -Wextra

SRCS = main.c fractol_hook.c mandelbrot.c fractol_image.c ft_rgb.c julia.c ft_exit.c \
		init_fractal.c calc_fractal.c env_init.c burningship.c dragon.c ft_lexique.c ft_move_julia.c ft_hook.c

OBJECT = $(SRCS:.c=.o)

HEADER = .

INCLUDE = fractol.h key_mlx.h

LIBFT = libft/libft.a

MLX_DIR = minilibx_macos/

FLAG_MLX = -lm -L $(MLX_DIR) -lmlx -framework OpenGL -framework AppKit

all: mlx $(LIBFT) $(NAME)

$(LIBFT):
	@make -C libft

mlx:
	@make -C minilibx_macos

%.o: %.c $(INCLUDE)
	$(CC) -o $@ -c $< $(FLAGS) -I$(HEADER) -I $(MLX_DIR)

$(NAME): $(OBJECT)
	$(CC) $(FLAGS) $(LIBFT) $(OBJECT) -o $(NAME) $(FLAG_MLX)

debug:
	$(CC) -g -o debug $(SRCS) -I$(HEADER) -I$(MLX_DIR) -I libft/ -L libft/ -lft -lm -L $(MLX_DIR) -lmlx -framework OpenGL -framework AppKit

clean:
	/bin/rm -rf $(OBJECT)
	@make clean -C libft
	@make clean -C minilibx_macos

fclean: clean
	/bin/rm -rf $(NAME)
	@make fclean -C libft
	@make clean -C minilibx_macos

re: fclean all

.PHONY: debug clean fclean re
