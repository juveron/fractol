/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 14:53:41 by juveron           #+#    #+#             */
/*   Updated: 2019/05/04 18:25:39 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		init_frac(t_env *e)
{
	if (e->frac_set == JULIA)
		e->projection = &julia;
	else if (e->frac_set == MANDELBROT)
		e->projection = &mandelbrot;
	else if (e->frac_set == BURNINGSHIP)
		e->projection = &burningship;
	else if (e->frac_set == DRAGON)
		e->projection = &dragon;
	return (1);
}

int		check_param(char *argv, t_env *e)
{
	if (ft_strequ(argv, "mandelbrot"))
		e->frac_set = MANDELBROT;
	else if (ft_strequ(argv, "julia"))
		e->frac_set = JULIA;
	else if (ft_strequ(argv, "burningship"))
		e->frac_set = BURNINGSHIP;
	else if (ft_strequ(argv, "dragon"))
		e->frac_set = DRAGON;
	else
		return (0);
	return (1);
}

int		ft_refresh(t_env *e)
{
	if (e->rend == 1)
	{
		e->projection(e);
		e->rend = 0;
	}
	mlx_put_image_to_window(e->init, e->win, e->image, 0, 0);
//	lexique(e);
	return (1);
}

void	ft_usage(void)
{
	ft_putstr_fd("Usage: ./fractol [mandelbrot ", 2);
	ft_putendl_fd("| julia | burningship | dragon]", 2);
}

int		main(int argc, char **argv)
{
	t_env *e;

	if (!(e = ft_memalloc(sizeof(t_env))))
		return (-1);
	if (argc == 2)
	{
		if (!check_param(argv[1], e))
		{
			ft_usage();
			properly_leaving(e, EXIT_FAILURE);
		}
		new_image(e);
		init_env(e);
		init_frac(e);
		mlx_hook(e->win, 2, 0, &key_hook, e);
		mlx_hook(e->win, 3, 0, &key_hook_release, e);
		mlx_hook(e->win, 4, 0, &mouse_hook, e);
		mlx_hook(e->win, 6, 0, &move_julia, e);
		mlx_loop_hook(e->init, &ft_refresh, e);
		mlx_loop(e->init);
	}
	else
		ft_usage();
	return (0);
}
