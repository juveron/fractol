/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rgb.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 15:20:43 by juveron           #+#    #+#             */
/*   Updated: 2019/04/29 14:55:52 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		ft_rgb(t_env *e)
{
	double			c;
	unsigned char	red;
	unsigned char	green;
	unsigned char	blue;

	c = (e->i + 1) * 0.1;
	red = cos(c + e->frac_init.color) * 127 + 128;
	green = cos(c + (e->frac_init.color * 4)) * 127 + 128;
	blue = cos(c + (e->frac_init.color * 4)) * 127 + 128;
	return ((int)((red << 16)) + (int)((blue << 8)) + (int)(green));
}
