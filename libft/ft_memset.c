/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 09:38:09 by juveron           #+#    #+#             */
/*   Updated: 2019/03/26 17:18:53 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *s, int c, size_t n)
{
	unsigned char			*cp;
	unsigned long			*lp;
	unsigned long			word;

	cp = s;
	c = c & 0xFF;
	while (n && (((unsigned long)cp & (sizeof(unsigned long) - 1)) != 0))
	{
		*(cp++) = (unsigned char)c;
		n--;
	}
	lp = (unsigned long *)((void *)cp);
	word = (unsigned long)c << 24 | (unsigned long)c << 16
		| (unsigned long)c << 8 | (unsigned long)c;
	word = word << 32 | word;
	while (n > sizeof(unsigned long))
	{
		*(lp++) = word;
		n -= sizeof(unsigned long);
	}
	cp = (unsigned char *)((void *)lp);
	while (n--)
		*(cp++) = (unsigned char)c;
	return (s);
}
