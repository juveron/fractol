/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/09 13:26:57 by juveron           #+#    #+#             */
/*   Updated: 2019/03/26 17:18:53 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	long int nblong;

	nblong = n;
	if (nblong < 0)
	{
		ft_putchar_fd('-', fd);
		nblong *= -1;
	}
	if (nblong > 9)
	{
		ft_putnbr_fd(nblong / 10, fd);
		ft_putnbr_fd(nblong % 10, fd);
	}
	else
		ft_putchar_fd(nblong + '0', fd);
}
