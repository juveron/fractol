/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 09:36:48 by juveron           #+#    #+#             */
/*   Updated: 2019/03/26 17:18:53 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *nptr)
{
	unsigned int	num;
	int				neg;
	int				c;

	num = 0;
	neg = 1;
	c = 0;
	while ((nptr[c] >= 9 && nptr[c] <= 13) || nptr[c] == 32)
		c++;
	if (nptr[c] == '-')
		neg = -1;
	if (nptr[c] == '-' || nptr[c] == '+')
		c++;
	while (ft_isdigit(nptr[c]))
	{
		if (num > (long)__INT_MAX__ + 1 && neg == -1)
			return (0);
		if (num > (long)__INT_MAX__ && neg == 1)
			return (-1);
		num = (num * 10) + (nptr[c] - 48);
		c++;
	}
	return ((int)num * neg);
}
