/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/09 12:56:53 by juveron           #+#    #+#             */
/*   Updated: 2019/03/26 17:18:53 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr(int n)
{
	long int nblong;

	nblong = n;
	if (nblong < 0)
	{
		ft_putchar('-');
		nblong *= -1;
	}
	if (nblong > 9)
	{
		ft_putnbr(nblong / 10);
		ft_putnbr(nblong % 10);
	}
	else
		ft_putchar(nblong + '0');
}
