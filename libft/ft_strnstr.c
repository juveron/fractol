/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 13:05:15 by juveron           #+#    #+#             */
/*   Updated: 2019/05/02 11:10:11 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *str, const char *to_find, size_t len)
{
	size_t	a;
	int		counter;

	if (to_find[0] == '\0')
		return ((char *)str);
	a = 0;
	while (str[a] != '\0' && a < len)
	{
		if (str[a] == to_find[0])
		{
			counter = 1;
			while (to_find[counter] != '\0'
				&& str[a + counter] == to_find[counter] && a + counter < len)
				counter++;
			if (to_find[counter] == '\0')
				return ((char *)&str[a]);
		}
		++a;
	}
	return (0);
}
