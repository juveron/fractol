/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/03 12:31:19 by juveron           #+#    #+#             */
/*   Updated: 2019/03/26 17:18:53 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dest, const char *src, size_t size)
{
	size_t		x;
	size_t		dest_size;
	const char	*sr;
	char		*dst;

	sr = src;
	dst = dest;
	x = size;
	while (*dst != '\0' && x-- != 0)
		dst++;
	dest_size = dst - dest;
	x = size - dest_size;
	if (x == 0)
		return (dest_size + ft_strlen(src));
	while (*sr != '\0')
	{
		if (x != 1)
		{
			*dst++ = *sr;
			x--;
		}
		sr++;
	}
	*dst = '\0';
	return (dest_size + (sr - src));
}
