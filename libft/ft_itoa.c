/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 17:45:49 by juveron           #+#    #+#             */
/*   Updated: 2019/03/26 17:18:53 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	get_ptr_len(int n)
{
	size_t x;

	x = 1;
	while (n /= 10)
		x++;
	return (x);
}

char			*ft_itoa(int n)
{
	char			*ptr;
	size_t			len;
	unsigned int	cpy;

	len = get_ptr_len(n);
	cpy = n;
	if (n < 0)
	{
		cpy = -n;
		len++;
	}
	if (!(ptr = ft_strnew(len)))
		return (NULL);
	ptr[--len] = cpy % 10 + '0';
	while (cpy /= 10)
		ptr[--len] = cpy % 10 + '0';
	if (n < 0)
		*(ptr + 0) = '-';
	return (ptr);
}
