/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 12:42:01 by juveron           #+#    #+#             */
/*   Updated: 2019/03/26 17:18:53 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*ptr;
	size_t	x;
	size_t	c;
	size_t	ps1;
	size_t	ps2;

	if (!s1 || !s2)
		return (NULL);
	ps1 = ft_strlen(s1);
	ps2 = ft_strlen(s2);
	ptr = ft_strnew(ps1 + ps2);
	if (!ptr)
		return (NULL);
	x = -1;
	c = -1;
	while (++x < ps1)
		*(ptr + x) = *(s1 + x);
	while (++c < ps2)
		*(ptr + x++) = *(s2 + c);
	return (ptr);
}
