/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 09:37:40 by juveron           #+#    #+#             */
/*   Updated: 2019/03/26 17:18:53 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	unsigned char	*idest;
	unsigned char	*isrc;

	idest = (unsigned char *)dest;
	isrc = (unsigned char *)src;
	while (n--)
	{
		*idest = *isrc;
		if (*isrc == (unsigned char)c)
			return ((void *)(idest + 1));
		idest++;
		isrc++;
	}
	return (NULL);
}
