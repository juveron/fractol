/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_julia.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/02 10:28:40 by juveron           #+#    #+#             */
/*   Updated: 2019/05/02 10:29:09 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		move_julia(int x, int y, t_env *e)
{
	int		current_x;
	int		current_y;

	if (e->actif == 1)
	{
		current_x = x - e->last_x;
		current_y = y - e->last_y;
		e->calc.cr += (double)-current_x / WIDTH;
		e->calc.ci += (double)-current_y / HEIGTH;
		if (x != e->last_x || y != e->last_y)
			e->rend = 1;
	}
	e->last_x = x;
	e->last_y = y;
	return (1);
}
