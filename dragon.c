/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dragon.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 18:20:58 by juveron           #+#    #+#             */
/*   Updated: 2019/04/30 15:54:09 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	dragon_calc_iter(t_env *e)
{
	e->calc.zi = e->calc.zr * e->calc.zi;
	e->calc.zi += e->calc.zi + e->calc.ci;
	e->calc.zr = sin(e->calc.zr2) - sin(e->calc.zi2) + e->calc.cr;
	e->calc.zi2 = e->calc.zi * e->calc.zi;
	e->calc.zr2 = e->calc.zr * e->calc.zr;
}

void	dragon(t_env *e)
{
	dragon_init(e);
	e->x = 0;
	while (e->x < e->frac_init.image_x)
	{
		e->y = 0;
		while (e->y < e->frac_init.image_y)
		{
			dragon_calc(e);
			e->i = 0;
			while (e->i < e->frac_init.i_max && e->calc.zr2 + e->calc.zi2 < 4)
			{
				dragon_calc_iter(e);
				e->i++;
			}
			if (e->i == e->frac_init.i_max)
				ft_put_pixel_for(e->x, e->y, 0, e);
			else
				ft_put_pixel(e->x, e->y, e);
			e->y++;
		}
		e->x++;
	}
}
