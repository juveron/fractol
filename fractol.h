/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 13:30:35 by juveron           #+#    #+#             */
/*   Updated: 2019/05/09 15:56:34 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# define WIDTH 1320
# define HEIGTH 770
# define ESC 53
# define SPACE 49
# define MAXITER 100

# include "libft/libft.h"
# include <math.h>
# include <mlx.h>


typedef struct		s_dvec2i
{
	double			x1;
	double			y1;
	double			x2;
	double			y2;
}					t_dvec2i;

typedef struct		s_frac
{
	int				image_x;
	int				image_y;
	int				color;
	int				i_max;
	double			zoom_x;

}					t_frac;

typedef struct		s_frac_calc
{
	double			cr;
	double			ci;
	double			zx;
	double			zx2;
	double			zy;
	double			zy2;
	double			zr;
	double			zr2;
	double			zi;
	double			zi2;
}					t_frac_calc;

typedef enum		e_frac_set
{
	JULIA,
	MANDELBROT,
	BURNINGSHIP,
	DRAGON,
}					t_frac_set;

typedef struct		s_env
{
	void			*init;
	void			*win;
	void			*image;
	int				*data;
	int				bpp;
	int				size_line;
	int				endian;
	t_dvec2i		cordo;
	t_frac			frac_init;
	t_frac_calc		calc;
	void			(*projection)();
	int				x;
	int				y;
	int				rend;
	char			*string;
	double			zoom;
	int				i;
	double			pos_x;
	double			pos_y;
	int				actif;
	int				last_x;
	int				last_y;
	int				pos_mouse_x;
	int				pos_mouse_y;
	t_frac_set		frac_set;
}					t_env;

void				dragon_init(t_env *e);
void				dragon_calc(t_env *e);
void				dragon(t_env *e);
void				julia_init(t_env *e);
void				julia_calc(t_env *e);
void				julia(t_env *e);
int					move_julia(int x, int y, t_env *e);
void				mandelbrot(t_env *e);
void				mandelbrot_init(t_env *e);
void				mandelbrot_calc(t_env *e);
void				burningship(t_env *e);
void				burningship_init(t_env *e);
void				burningship_calc(t_env *e);
void				init_env(t_env *e);
int					key_hook1(int key, t_env *e);
int					key_hook(int key, t_env *e);
int					mouse_hook(int key, int x, int y, t_env *e);
int					ft_rgb(t_env *e);
int					lexique(t_env *e);
int					image_to_window(t_env *e);
void				properly_leaving(t_env *e, int exit_frac);
int					move_julia(int x, int y, t_env *e);
void				new_image(t_env *e);
void				ft_put_pixel(int x, int y, t_env *e);
void				ft_put_pixel_for(int x, int y, int color, t_env *e);
int					key_hook_release(int key, t_env *e);

#endif
