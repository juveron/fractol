/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   burningship.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 17:30:53 by juveron           #+#    #+#             */
/*   Updated: 2019/05/02 11:25:51 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	burningship_calc_iter(t_env *e)
{
	e->calc.zi = fabs(e->calc.zr) * fabs(e->calc.zi);
	e->calc.zi += fabs(e->calc.zi) + e->calc.ci;
	e->calc.zr = e->calc.zr2 - e->calc.zi2 + e->calc.cr;
	e->calc.zi2 = fabs(e->calc.zi) * fabs(e->calc.zi);
	e->calc.zr2 = fabs(e->calc.zr) * fabs(e->calc.zr);
}

void	burningship(t_env *e)
{
	mandelbrot_init(e);
	e->x = 0;
	while (e->x < e->frac_init.image_x)
	{
		e->y = 0;
		e->calc.cr = ((double)e->x / HEIGTH) * (e->cordo.x2 - e->cordo.x1)
			/ e->zoom + e->cordo.x1 + e->pos_x;
		while (e->y < e->frac_init.image_y)
		{
			burningship_calc(e);
			e->i = 0;
			while (e->i < e->frac_init.i_max && e->calc.zr2 + e->calc.zi2 < 4)
			{
				burningship_calc_iter(e);
				e->i++;
			}
			if (e->i == e->frac_init.i_max)
				ft_put_pixel_for(e->x, e->y, 0, e);
			else
				ft_put_pixel(e->x, e->y, e);
			e->y++;
		}
		e->x++;
	}
}
