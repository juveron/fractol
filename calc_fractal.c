/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calc_fractal.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 14:51:47 by juveron           #+#    #+#             */
/*   Updated: 2019/05/02 11:26:43 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	julia_calc(t_env *e)
{
	e->calc.zi = (double)e->y / HEIGTH * (e->cordo.y2 - e->cordo.y1)
		/ e->zoom + e->cordo.y1 + e->pos_y;
	e->calc.zr = (double)e->x / HEIGTH * (e->cordo.x2 - e->cordo.x1)
		/ e->zoom + e->cordo.x1 + e->pos_x;
	e->calc.zr2 = e->calc.zr * e->calc.zr;
	e->calc.zi2 = e->calc.zi * e->calc.zi;
}

void	mandelbrot_calc(t_env *e)
{
	e->calc.ci = (double)e->y / HEIGTH * (e->cordo.y2 - e->cordo.y1)
		/ e->zoom + e->cordo.y1 + e->pos_y;
	e->calc.cr = (double)e->x / HEIGTH * (e->cordo.x2 - e->cordo.x1)
		/ e->zoom + e->cordo.x1 + e->pos_x;
	e->calc.zr = 0.0;
	e->calc.zi = 0.0;
	e->calc.zr2 = e->calc.zr * e->calc.zr;
	e->calc.zi2 = e->calc.zi * e->calc.zi;
}

void	burningship_calc(t_env *e)
{
	e->calc.ci = ((double)e->y / HEIGTH) * (e->cordo.y2 - e->cordo.y1)
		/ e->zoom + e->cordo.y1 + e->pos_y;
	e->calc.zr = 0.0;
	e->calc.zi = 0.0;
	e->calc.zr2 = e->calc.zr * e->calc.zr;
	e->calc.zi2 = e->calc.zi * e->calc.zi;
}

void	dragon_calc(t_env *e)
{
	e->calc.zi = (double)e->y / HEIGTH * (e->cordo.y2 - e->cordo.y1)
		/ e->zoom + e->cordo.y1 + e->pos_y;
	e->calc.zr = (double)e->x / HEIGTH * (e->cordo.x2 - e->cordo.x1)
		/ e->zoom + e->cordo.x1 + e->pos_x;
	e->calc.zr2 = e->calc.zr * e->calc.zr;
	e->calc.zi2 = e->calc.zi * e->calc.zi;
}
