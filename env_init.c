/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_init.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 15:17:50 by juveron           #+#    #+#             */
/*   Updated: 2019/04/30 15:56:26 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	init_env(t_env *e)
{
	e->frac_init.image_x = WIDTH;
	e->frac_init.image_y = HEIGTH;
	e->calc.cr = -0.162;
	e->calc.ci = 1.04;
	e->zoom = 1;
	e->actif = 0;
	e->pos_x = 0;
	e->pos_y = 0;
	e->frac_init.i_max = 50;
	e->frac_init.color = 9830424;
	e->rend = 1;
}
