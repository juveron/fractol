/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol_hook.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 14:53:06 by juveron           #+#    #+#             */
/*   Updated: 2019/05/04 18:18:19 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include "key_mlx.h"

int		ft_zoom(int x, int y, double zoom, t_env *e)
{
	double	size_x;
	double	size_y;
	double	n_size_x;
	double	n_size_y;

	size_x = (e->cordo.x2 - e->cordo.x1) / e->zoom;
	size_y = (e->cordo.y2 - e->cordo.y1) / e->zoom;
	e->zoom *= zoom;
	n_size_x = (e->cordo.x2 - e->cordo.x1) / e->zoom;
	n_size_y = (e->cordo.y2 - e->cordo.y1) / e->zoom;
	e->pos_x -= (double)x / HEIGTH * (n_size_x - size_x);
	e->pos_y -= (double)y / HEIGTH * (n_size_y - size_y);
	return (1);
}

int		mouse_hook(int key, int x, int y, t_env *e)
{
	if (key == 4)
		ft_zoom(x, y, 2, e);
	else if (key == 5 && e->zoom >= 1)
		ft_zoom(x, y, 0.5, e);
	else if (key == 1 && e->projection == &julia)
		e->actif = e->actif == 1 ? 0 : 1;
	if (key == 4 || key == 5 || key == 1)
		e->rend = 1;
	return (1);
}

int		key_hook1(int key, t_env *e)
{
	if (key == ESC)
		properly_leaving(e, EXIT_SUCCESS);
	else if (key == KEY_UP)
		e->pos_y -= 0.1 / e->zoom;
	else if (key == KEY_DOWN)
		e->pos_y += 0.1 / e->zoom;
	else if (key == KEY_LEFT)
		e->pos_x -= 0.1 / e->zoom;
	else if (key == KEY_RIGHT)
		e->pos_x += 0.1 / e->zoom;
	else if (key == KEY_R)
		e->frac_init.i_max += 10;
	else if (key == KEY_T && e->frac_init.i_max > 10)
		e->frac_init.i_max -= 10;
	else if (key == KEY_Q)
		init_env(e);
	if (key == ESC || key == KEY_UP || key == KEY_DOWN || key == KEY_LEFT
		|| key == KEY_RIGHT || key == KEY_Q)
		e->rend = 1;
	return (1);
}

int		key_hook2(int key, t_env *e)
{
	if (key == KEY_SPACEBAR)
	{
		if (e->projection == &mandelbrot)
		{
			e->projection = &julia;
			init_env(e);
		}
		else if (e->projection == &julia)
		{
			e->projection = &burningship;
			init_env(e);
		}
		else if (e->projection == &burningship)
		{
			e->projection = &dragon;
			init_env(e);
		}
		else
		{
			e->projection = &mandelbrot;
			init_env(e);
		}
	}
	return (1);
}

int		key_hook(int key, t_env *e)
{
	key_hook1(key, e);
	key_hook2(key, e);
	if (key == KEY_K)
		e->frac_init.color += 10000;
	else if (key == KEY_L)
		e->frac_init.color -= 10000;
	if (key == KEY_K || key == KEY_L || key == KEY_SPACEBAR)
		e->rend = 1;
	return (1);
}
