/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol_image.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 14:52:27 by juveron           #+#    #+#             */
/*   Updated: 2019/04/29 16:29:12 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_put_pixel(int x, int y, t_env *e)
{
	if (x > 0 && x < WIDTH && y > 0 && y < HEIGTH)
		e->data[y * (e->size_line / 4) + x] = ft_rgb(e);
}

void	ft_put_pixel_for(int x, int y, int color, t_env *e)
{
	if (x > 0 && x < WIDTH && y > 0 && y < HEIGTH)
		e->data[y * (e->size_line / 4) + x] = color;
}

void	new_image(t_env *e)
{
	e->init = mlx_init();
	e->win = mlx_new_window(e->init, WIDTH, HEIGTH, "fractol");
	e->image = mlx_new_image(e->init, WIDTH, HEIGTH);
	e->data = (int *)mlx_get_data_addr(e->image, &(e->bpp), &(e->size_line),
		&(e->endian));
}
