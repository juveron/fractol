/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/26 12:01:47 by juveron           #+#    #+#             */
/*   Updated: 2019/05/04 18:06:36 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	properly_leaving(t_env *e, int exit_frac)
{
	if (e->image)
		mlx_destroy_image(e->init, e->image);
	if (e->win)
		mlx_destroy_window(e->init, e->win);
	free(e);
	exit(exit_frac);
}
