/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_fractal.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 14:48:27 by juveron           #+#    #+#             */
/*   Updated: 2019/05/02 10:49:39 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	julia_init(t_env *e)
{
	e->cordo.x1 = -1.6;
	e->cordo.y1 = -1.2;
	e->cordo.x2 = 1.1;
	e->cordo.y2 = 1.2;
}

void	mandelbrot_init(t_env *e)
{
	e->cordo.x1 = -2.1;
	e->cordo.y1 = -1.2;
	e->cordo.x2 = 0.6;
	e->cordo.y2 = 1.2;
}

void	burningship_init(t_env *e)
{
	e->cordo.x1 = -2.1;
	e->cordo.y1 = -1.2;
	e->cordo.x2 = 0.6;
	e->cordo.y2 = 1.2;
}

void	dragon_init(t_env *e)
{
	e->cordo.x1 = -1.6;
	e->cordo.y1 = -1.2;
	e->cordo.x2 = 1.1;
	e->cordo.y2 = 1.2;
	e->calc.cr = -0.328000;
	e->calc.ci = 0.734000;
}
