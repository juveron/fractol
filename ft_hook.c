/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hook.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/02 12:38:04 by juveron           #+#    #+#             */
/*   Updated: 2019/05/02 12:38:49 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include "key_mlx.h"

int		key_hook_release(int key, t_env *e)
{
	if (key == KEY_R || key == KEY_T)
		e->rend = 1;
	return (1);
}
