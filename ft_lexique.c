/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lexique.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: juveron <juveron@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/30 13:31:40 by juveron           #+#    #+#             */
/*   Updated: 2019/05/04 18:14:05 by juveron          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		lexique1(t_env *e)
{
	e->string = ft_itoa(e->frac_init.i_max);
	if (e->projection == &julia)
		mlx_string_put(e->init, e->win, 20, 20, 0xFFFFFF, "JULIA");
	else if (e->projection == &mandelbrot)
		mlx_string_put(e->init, e->win, 20, 20, 0xFFFFFF, "MANDELBROT");
	else if (e->projection == &burningship)
		mlx_string_put(e->init, e->win, 20, 20, 0xFFFFFF, "BURNINGSHIP");
	else if (e->projection == &dragon)
		mlx_string_put(e->init, e->win, 20, 20, 0xFFFFFF, "DRAGON");
	mlx_string_put(e->init, e->win, 20, 40, 0xFFFFFF, "current iteration :");
	mlx_string_put(e->init, e->win, 220, 40, 0xFFFFFF, e->string);
	free(e->string);
	return (1);
}

int		lexique(t_env *e)
{
	mlx_string_put(e->init, e->win, 1570, 70, 0xFFFFFF, "Exit = Esc");
	mlx_string_put(e->init, e->win, 1570, 90, 0xFFFFFF, "up = ^");
	mlx_string_put(e->init, e->win, 1570, 110, 0xFFFFFF, "down = v");
	mlx_string_put(e->init, e->win, 1570, 130, 0xFFFFFF, "left = <");
	mlx_string_put(e->init, e->win, 1570, 150, 0xFFFFFF, "right = >");
	mlx_string_put(e->init, e->win, 1570, 170, 0xFFFFFF, "zoom = molette up");
	mlx_string_put(e->init, e->win, 1570, 190, 0xFFFFFF,
		"unzoom = molette down");
	mlx_string_put(e->init, e->win, 1570, 210, 0xFFFFFF
		, "more iter = r");
	mlx_string_put(e->init, e->win, 1570, 230, 0xFFFFFF
		, "less iter = t");
	mlx_string_put(e->init, e->win, 1570, 250, 0xFFFFFF, "reset = q");
	mlx_string_put(e->init, e->win, 1570, 270, 0xFFFFFF, "palet up = k");
	mlx_string_put(e->init, e->win, 1570, 290, 0xFFFFFF,
		"palet down l");
	mlx_string_put(e->init, e->win, 1570, 310, 0xFFFFFF,
		"switch fractal = space");
	mlx_string_put(e->init, e->win, 1570, 330, 0xFFFFFF,
		"activeted julia's move = left click");
	lexique1(e);
	return (1);
}
